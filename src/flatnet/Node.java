package flatnet;

import java.awt.*;
import java.util.ArrayList;

/**
 * Flat Net is an attempted slow-backpropagation network.
 * Each output node learns its function surface a bit like a Kohonen network.
 * Then, during rumination, every node tries to become more linear by flattening its function surface, 
 * transferring adjustments to its upstream and downstream nodes.
 * As every neighbor node also tries to flatten its own function surface, this creates
 * a struggle (and hopefully negotiation) between nodes, like springs in a physics simulation.
 * 
 * The hope is that the whole network will become more linear with rumination.
 * 
 * @author jcatkeson, February 2009
 */
public class Node extends Node_Base {
  /* *************************************************************************************************** */

  PointNd_Mov prev_closest = null;// new PointNd_Mov(ndims);
  PointNd_Mov closest = null;// new PointNd_Mov(ndims);
  PointNd infire = new PointNd(ndims);
  PointNd avg_nd = new PointNd(ndims);
  //PointNd[] avg_lo = new PointNd[ndims], avg_hi = new PointNd[ndims];
  //PointNd[] offctr = new PointNd[2], offctr_cross = new PointNd[2];
  double num_trials = 0.0;
  double avg_real_slope = 0.0;
  double avg_test_slope = 0.0;
  PointNd_List plist;
  /* *************************************************************************************************** */
  Roto_Plane planeform;
  PointNd normal = new PointNd(ndims);
  PointNd desire = new PointNd(ndims);
  /* *************************************************************************************************** */

  public Node() {
    super();
    plist = new PointNd_List(0);
    planeform = new Roto_Plane(ndims);
    planeform.Randomize(-0.5, 0.5);
    //planeform.Randomize(-3.0, 3.0);
  }
  /* *************************************************************************************************** */

  public class PointNd_List extends ArrayList<PointNd_Mov> {

    public PointNd_List(int newsize) {
      xorg = 200;
      yorg = 200;
      PointNd avg = new PointNd(ndims);
      for (int cnt = 0; cnt < newsize; cnt++) {
        PointNd_Mov pnt = new PointNd_Mov(ndims);
        pnt.Randomize(-1.0, 1.0);
        this.add(pnt);
      }
      avg.Multiply(newsize);// experiment to move all the points to the centroid
    }
    /* *************************************************************************************************** */

    public void Get_Average(PointNd ret_avg) {
      ret_avg.Clear();
      for (PointNd pnt : this) {
        ret_avg.Add(pnt);
      }
      ret_avg.Multiply(1.0 / (double) this.size());
    }
    /* *************************************************************************************************** */

    public void CheckNAN() {
      for (PointNd pnt : this) {
        pnt.CheckNAN();
      }
    }
  }/* PointNd_List */
  /* *************************************************************************************************** */


  public class Roto_Plane extends PlaneNd {
    // the purpose of this class is to represent a sigmoid plane, to fit it to points, and to fit points to it.

    private PointNd pingvec;
    public double rangemin, rangectr, rangemax;
    PointNd running_avg = new PointNd(ndims);
    public double above, below;
    /* *************************************************************************************************** */

    public Roto_Plane(int num_dims) {
      super(num_dims);
      pingvec = new PointNd(ndims);
      rangemin = -1.0;
      rangectr = 0.0;
      rangemax = 1.0;
      above = 0.0;
      below = 0.0;
    }
    /* *************************************************************************************************** */

    public double ActFun(double xin) {
      double OutVal;
      if (false) {
        return xin;
      } else {
        OutVal = xin / Math.sqrt(1.0 + xin * xin);/* symmetrical sigmoid function in range -1.0 to 1.0. */
        return OutVal;
      }
      /*
       * double power = 2.0;
      OutVal = xin / Math.pow(1 + Math.abs(Math.pow(xin, power)), 1.0 / power);
       */
    }
    /* *************************************************************************************************** */

    public double Reverse_ActFun(double xin) {
      double OutVal;// from http://www.quickmath.com/webMathematica3/quickmath/page.jsp?s1=equations&s2=solve&s3=basic
      // reverse, inverse of sigmoid is:
      OutVal = xin / (Math.sqrt(Math.abs(xin * xin - 1.0)));
      return OutVal;
    }
    /* *************************************************************************************************** */

    public double sigmoid_deriv(double Value) { /* modified for default return  -dbr */
      /* Given the unit's activation value and sum of weighted inputs,
       *  compute the derivative of the activation with respect to the sum.
       *  Defined types are SIGMOID (-1 to +1) and ASYMSIGMOID (0 to +1).
       * */
      //Value *= (1.17 * 1.01);
      Value *= (1.17 * 1.0095);// is 1.181115
      Value = (Value + 1.0) / 2;
//    Value = (Value * 0.5)+0.5;
      double SigmoidPrimeOffset = 0.1;
      /*  asymmetrical sigmoid function in range 0.0 to 1.0.  */
      double returnval = (SigmoidPrimeOffset + (Value * (1.0 - Value)));
      returnval *= 2.857142857142857;// modify to fit 1:1 slope of sigmooid
      return returnval * 1.0;
      //return ((SigmoidPrimeOffset + (Value * (1.0 - Value))));
    }
    /* *************************************************************************************************** */

    public void Ping(PointNd pnt) {
      //sigmoid_deriv
      double phgt = pnt.loc[ninputs];
      // first get the height of the plane at this loc,
      double result;
      result = this.Get_Height(pnt);
      result = this.ActFun(result);
      // corrector is the distance from the sigmoid plane TOWARD this point's height
      double corr = phgt - result;

      double bell = this.sigmoid_deriv(phgt);
      corr *= bell;

      // now create the recognition vector, based on this pnt's position, and 1.0 * this plane's height offset dimension
      for (int cnt = 0; cnt < ninputs; cnt++) {
        pingvec.loc[cnt] = pnt.loc[cnt];
      }
      pingvec.loc[ninputs] = 1.0;
      Train_Inlinks(pingvec, ndims, 0.01, corr);

      double pdrop = (phgt * 0.1);
      this.rangectr = (this.rangectr * 0.9) + pdrop;
      if (true) {
        this.rangemax = ((this.rangemax - this.rangectr) * 0.9) + this.rangectr;
        this.rangemin = ((this.rangemin - this.rangectr) * 0.9) + this.rangectr;
        if (phgt < this.rangemin) {
          this.rangemin = phgt;
        }
        if (phgt > this.rangemax) {
          this.rangemax = phgt;
        }
      }
      /* keep an approximate centroid for the cloud of points that hit me */
      for (int cnt = 0; cnt < ninputs; cnt++) {
        running_avg.loc[cnt] *= 0.9;
        running_avg.loc[cnt] += 0.1 * pnt.loc[cnt];
      }
      above *= 0.9;
      below *= 0.9;
      if (pnt.loc[ninputs] > 0.0) {
        above += 0.1;
      } else if (pnt.loc[ninputs] < 0.0) {
        below += 0.1;
      }
      //this.Balance();
    }
    /* *************************************************************************************************** */

    public double Get_Sigmoid_Height(PointNd pnt) {
      double hgt = this.Get_Height(pnt);
      hgt = this.ActFun(hgt);
      return hgt;
    }
    /* *************************************************************************************************** */

    public void Balance() {
      double sub_ratio = above;// ratio of above-zero to total
      if (sub_ratio < 0.25 || 0.75 < sub_ratio) {
        double midval = this.Get_Sigmoid_Height(running_avg);
        double corr = 0.0 - midval;
        // whoops, need to make average be like pingvec, for the training
        running_avg.loc[ninputs] = 1.0;
        Train_Inlinks(running_avg, ndims, 0.1, corr);// was lrate of 0.01
      }
      if (false) {// another experiment, try to flatten the plane.
        for (int cnt = 0; cnt < ninputs; cnt++) {
          if (Math.abs(this.loc[cnt]) > 0.7) {
            this.loc[cnt] *= 0.999;
          }
        }
      }
    }
    /* *************************************************************************************************** */

    public void Attract_Point(PointNd pnt, PointNd pdesire) {
      double shadow_hgt = this.Get_Height(pnt);// height on raw plane at this point's position.
      double sigmoid_shadow_hgt = this.ActFun(shadow_hgt);// height on sigmoid plane at this point's position.
      //double sigmoid_shadow_hgt = this.Get_Sigmoid_Height(pnt);// height on sigmoid plane at this point's position.

      //sigmoid_shadow_hgt = shadow_hgt;
      double delta_hgt = sigmoid_shadow_hgt - pnt.loc[ninputs];// distance from this point to sigmoid plane.

      PointNd nrm = new PointNd(ndims);
      this.Plane_Ramp_To_Normal(nrm);// get normal to raw plane.
      nrm.Unitize();
      PointNd delta = new PointNd(ndims);// delta is the vector from the point to its place on the (sigmoid) plane
      delta.loc[ninputs] = delta_hgt;
      double corrlen = delta.Dot_Product(nrm);// project delta onto normal, to get straight distance to plane.
      delta.Copy_From(nrm);// multiply unit normal by corrector length to get correction vector.
      delta.Multiply(corrlen);
      pdesire.Copy_From(delta);
      if (false) {// flee the centroid
        delta.Copy_From(pnt);
        delta.Subtract(running_avg);
        if (delta.Magnitude(ninputs) <= 0.7) {
          delta.Multiply(0.01);
          delta.loc[ninputs] = 0.0;
          pdesire.Add(delta);
        }
      }
      if (false) {// flee the crack
        PointNd steep = new PointNd(ndims);
        normal.Get_Steepest(steep);
        steep.loc[ninputs] = 0;
        steep.Unitize();
        double sgn = Math.signum(pnt.loc[ninputs]);
        steep.Multiply(sgn);
        steep.Multiply(0.001);
        pdesire.Add(steep);
      }
      double range = Math.abs(this.rangemax - this.rangemin);
      double unitrange = (2.0 - range) / 2.0;// is the range as 0 to 1.0
      double finalrange = (0.9 * unitrange) + 0.1;//  this way the flattest ranges have the strongest self-inspired vertical attraction.

      double vfactor = finalrange;
      vfactor = 0.8;// works better with 2 layers
      vfactor = 0.09;// works better with 7 layers, and maybe 3
      //vfactor = 0.07;// test with 7
      vfactor *= 0.3;
      pdesire.loc[ninputs] *= vfactor;

      double jitamp = 0.0001;
      pdesire.Jitter(-jitamp, jitamp);
      //pdesire.loc[ninputs] = 0.0;
    }
    /* *************************************************************************************************** */

    public void Train_Inlinks(PointNd invec, int ninputs_local, double lrate, double corrector) {
      double invec_squared = invec.Magnitude_Squared(ninputs_local);
      if (invec_squared == 0.0) {
        invec_squared = fudge;
      }
      for (int cnt = 0; cnt < ninputs_local; cnt++) {
        double adj = (invec.loc[cnt] / invec_squared);// unitary adjustment tool
        adj = adj * corrector * lrate;
        this.loc[cnt] += adj;
      }
    }/* Train_Inlinks */
    /* *************************************************************************************************** */


    public void Plot_Gradient(Graphics2D g2) {
      /* all about the gradient for display */
      // , PointNd grad0, PointNd grad1
      double hgt = this.loc[0];
      double grad_x0;
      double grad_y0;
      double grad_x1, grad_y1;
      PointNd steepest = new PointNd(ndims);
      normal.Get_Steepest(steepest);
      double[] ratios = new double[ninputs];// the ratios are NOT the inverse slopes.  they are from the steepest line.
      for (int cnt = 0; cnt < ninputs; cnt++) {
        ratios[cnt] = steepest.loc[cnt] / steepest.loc[ninputs];// inverse slope for each shadow of the steepest
      }
      if (true) {
        double offset = planeform.loc[ninputs];
        //offset *= Bounds.Rad(ninputs);
        double height0 = -1.0 - offset;
        double height1 = 1.0 - offset;
        double brad = Bounds.Rad(ninputs);
        //gradx0 = (int) (brad * (height0 * ratios[0])); grad_y0 = (int) (brad * (height0 * ratios[1])); grad_x1 = (int) (brad * (height1 * ratios[0])); grad_y1 = (int) (brad * (height1 * ratios[1]));
        height0 *= brad;
        height1 *= brad;
        grad_x0 = (int) ((height0 * ratios[0]));
        grad_y0 = (int) ((height0 * ratios[1]));
        grad_x1 = (int) ((height1 * ratios[0]));
        grad_y1 = (int) ((height1 * ratios[1]));
      }
      Color startColor = new Color(0.0f, 0.0f, 1.0f);//Color startColor = Color.blue;
      Color endColor = new Color(1.0f, 0.0f, 0.0f);//Color endColor = Color.red;

      GradientPaint gradient;
      gradient = new GradientPaint(xorg + (int) grad_x0, yorg + (int) grad_y0, startColor, xorg + (int) grad_x1, yorg + (int) grad_y1, endColor);// A non-cyclic gradient
      g2.setPaint(gradient);
      g2.fillRect(xorg + (int) Bounds.minmax[0][0], yorg + (int) Bounds.minmax[0][1], (int) Bounds.Wdt(), (int) Bounds.Hgt());
      g2.setColor(Color.white);
      g2.drawLine(xorg + (int) grad_x0, yorg + (int) grad_y0, xorg + (int) grad_x1, yorg + (int) grad_y1);
    }
  }
  /* *************************************************************************************************** */

  @Override
  public void Draw_3d(Graphics gr) {
    Graphics2D g2 = (Graphics2D) gr;
    if (plist.size() == 0) {
      return;
    } else {// if (ndims == 3) {
      this.planeform.Plot_Gradient(g2);

      int dim3 = 2;
      // now illustrate all the data points.
      int ptsz = 8;
      int halfptsz = ptsz / 2;
      for (int cnt = 0; cnt < plist.size(); cnt++) {
        PointNd_Mov pnt = plist.get(cnt);
        float pntband = (float) ((pnt.loc[ninputs] + 1.0) / 2.0);// map to range 0.0 - 1.0

        float red = pntband, blue = (float) 1.0 - pntband;
        if (blue < 0) {
          blue = 0;
        } else if (blue > 1.0) {
          blue = 1;
        }
        if (red < 0) {
          red = 0;
        } else if (red > 1.0) {
          red = 1;
        }
        Color pnt_color = new Color(red, 0.0f, blue);
        g2.setColor(pnt_color);
        g2.fillRect(xorg + (int) (Bounds.Rad(0) * pnt.loc[0] - halfptsz), yorg + (int) (Bounds.Rad(1) * pnt.loc[1] - halfptsz), ptsz, ptsz);
        g2.setColor(Color.black);
        g2.drawRect(xorg + (int) (Bounds.Rad(0) * pnt.loc[0] - halfptsz), yorg + (int) (Bounds.Rad(1) * pnt.loc[1] - halfptsz), ptsz, ptsz);
        if (true) {
          double factor = 100.0;
          g2.setColor(Color.magenta);/* draw the movement line */
          g2.drawLine(
                  (int) (xorg + Bounds.Rad(0) * pnt.loc[0]),
                  (int) (yorg + Bounds.Rad(1) * pnt.loc[1]),
                  (int) (xorg + Bounds.Rad(0) * (pnt.loc[0] + pnt.delta[0] * factor)),
                  (int) (yorg + Bounds.Rad(1) * (pnt.loc[1] + pnt.delta[1] * factor)));
        }
      }
      if (closest != null) {// draw the point we hit
        double factor = 1.0;
        g2.setColor(Color.white);
        g2.drawRect(xorg + (int) (Bounds.Rad(0) * closest.loc[0] - halfptsz), yorg + (int) (Bounds.Rad(1) * closest.loc[1] - halfptsz), ptsz, ptsz);

        g2.setColor(Color.magenta);
        g2.drawLine(// draw desire vector
                (int) (xorg + Bounds.Rad(0) * closest.loc[0]),
                (int) (yorg + Bounds.Rad(1) * closest.loc[1]),
                (int) (xorg + Bounds.Rad(0) * (closest.loc[0] + desire.loc[0] * factor)),
                (int) (yorg + Bounds.Rad(1) * (closest.loc[1] + desire.loc[1] * factor)));
      }
      if (true) {// (!this.Learning_Mode) {// plot actual infire
        //g2.setColor(Color.white);
        //g2.fillRect(xorg + (int) (Bounds.Rad(0) * infire.loc[0] - halfptsz), yorg + (int) (Bounds.Rad(1) * infire.loc[1] - halfptsz), ptsz, ptsz);

        g2.setColor(Color.green);
        int xmid = xorg + (int) (Bounds.Rad(0) * infire.loc[0]);
        int ymid = yorg + (int) (Bounds.Rad(1) * infire.loc[1]);

        int x0 = xmid - halfptsz;// xorg + (int) (Bounds.Rad(0) * infire.loc[0] - halfptsz);
        int y0 = ymid - halfptsz;//yorg + (int) (Bounds.Rad(1) * infire.loc[1] - halfptsz);
        int x1 = x0 + ptsz;
        int y1 = y0 + ptsz;
        g2.drawLine(xmid, y0, xmid, y1);// vertical line
        g2.drawLine(x0, ymid, x1, ymid);// horizontal line
      }
    }
    if (true) {// print stats
      String pntcnt = Integer.toString(plist.size());
      int strhgt = 10;
      g2.setColor(Color.white);
      g2.drawString(pntcnt, xorg, yorg + strhgt + (int) Bounds.minmax[1][1]);
      if (false) {// full vector dump of rotoplane
        for (int dcnt = 0; dcnt < ndims; dcnt++) {
          double ramp = this.planeform.loc[dcnt];
          int ydep = strhgt * dcnt;
          g2.drawString(Double.toString(ramp), xorg + 12, yorg + ydep + (int) Bounds.minmax[1][1]);
        }
      }
    }
  }
  /* *************************************************************************************************** */

  @Override
  public void Increase_Dims() {
    super.Increase_Dims();
    planeform = new Roto_Plane(ndims);
    infire = new PointNd(ndims);
    avg_nd = new PointNd(ndims);
    normal = new PointNd(ndims);
    desire = new PointNd(ndims);
  }
  /* *************************************************************************************************** */

  @Override
  public void Add_Inlink(Node_Base upstreamer) {
    In_Link inlink = new In_Link();
    Node_Base usnode = null;
    try {
      usnode = (Node_Base) upstreamer;
    } catch (Throwable e) {
      boolean nop = true;
    }
    inlink.upstreamer = usnode;
    inlink.downstreamer = this;
    usnode.outlinks.add(inlink);
    this.inlinks.add(inlink);
    this.Increase_Dims();
  }
  /* *************************************************************************************************** */

  public void Load_Infires(PointNd infire) {
    int ndims_local = inlinks.size();// this.ndims;
    for (int cnt = 0; cnt < ndims_local; cnt++) {
      // we want to go through each inlink, and save its values in a point's dimension
      In_Link inlink = inlinks.get(cnt);
      infire.loc[cnt] = inlink.fireval;
    }
  }
  /* *************************************************************************************************** */
  public double maxmindist = java.lang.Double.NEGATIVE_INFINITY;
  public int itcnt = 0;

  public PointNd_Mov Find_Closest(int dimensions) {
    PointNd_Mov closest_ptr = null;
    double dist = 0.0, mindist = java.lang.Double.POSITIVE_INFINITY;
    int pcnt = 0;
    if (true) {
      for (pcnt = 0; pcnt < this.plist.size(); pcnt++) {
        PointNd_Mov pnt = this.plist.get(pcnt);
        dist = infire.Get_Distance(pnt, dimensions);
        if (dist == java.lang.Double.POSITIVE_INFINITY) {
          boolean nop = true;
        }
        if (dist < mindist) {
          mindist = dist;
          closest_ptr = pnt;
        }
      }
    }
    if (closest_ptr == null) {
      boolean noop = true;
    }
    closest_ptr.Refresh();
    if (mindist >= 0.2) {
      boolean noop = true;
    }
    if (mindist > maxmindist) {
      maxmindist = mindist;
    }
    if (itcnt % 10000 == 0) {
      //System.out.println(" it:" + itcnt + " mm:" + maxmindist);
    }
    return closest_ptr;
  }
  /* *************************************************************************************************** */

  @Override
  public void Create_Path() {// for initialization of dystal-style patches
    PointNd_Mov newpnt = new PointNd_Mov(ndims);
    Load_Infires(newpnt);// load infires into inputs of newpnt
    newpnt.loc[ninputs] = Logic.wheel.nextDouble() * 2.0 - 1.0;// we need to expand this to the size of the plot box.
    this.plist.add(newpnt);
    closest = newpnt;
    prev_closest = closest;
    outfire = this.Actfun(closest.loc[ninputs]);
  }
  /* *************************************************************************************************** */

  @Override
  public void Collect_And_Fire() {
    Load_Infires(infire);
    prev_closest = closest;
    closest = this.Find_Closest(ninputs);
    planefire = this.planeform.Get_Sigmoid_Height(infire);
    boolean lmode = Learning_Mode;
    //lmode = true;// digital testing mode
    if (lmode) {
      // get radius of infire from closest, and create output
      double delta = infire.Get_Distance(closest, ninputs);
      outfire = this.Actfun(closest.loc[ninputs]);
      if (false) {// any miss from hitting the point exactly still carries some information.
        if (delta <= 1.0) {
          outfire *= (1.0 - delta);// cone distribution.  probably better to make it gaussian.
          outfire += planefire * delta;
        } else {
          outfire = planefire;
        }
      }
    } else {
      outfire = planefire;
    }
  }
  /* *************************************************************************************************** */

  @Override
  public void Distribute_Outfire() {
    for (In_Link outlink : outlinks) {
      outlink.fireval = outfire;
    }
  }
  /* *************************************************************************************************** */

  @Override
  public void Collect_Desire_Backward() {
    double avgd = 0.0;
    for (In_Link outlink : outlinks) {
      avgd += outlink.desire;
    }
    avgd /= outlinks.size();
    Apply_Desire_To_Closest(avgd);
  }
  /* *************************************************************************************************** */

  @Override
  public void Apply_Desire_To_Closest(double desire) {
    boolean fullfire = Layers.fullfire;
    if (this.Learning_Mode) {
      if (!fullfire) {
        Apply_Desire_To_Point(this.closest, desire);
      } else {
        Apply_Desire_To_Point(this.prev_closest, desire);
      }
    }
  }
  /* *************************************************************************************************** */

  @Override
  public double Actfun(double rawval) {
    double retval = rawval;
    return retval;
  }
  /* *************************************************************************************************** */

  @Override
  public void Set_Outfire(double newoutfire) {
    super.Set_Outfire(newoutfire);
    this.closest.loc[ninputs] = this.outfire;
  }
  /* *************************************************************************************************** */
  int shave = 100;

  @Override
  public void Run_Cycle() {
    this.Collect_And_Fire();
    Age_Points();
    if (this.Learning_Mode) {
      //if (closest.stress > 0.02) {
      if ((itcnt + 1) % shave == 0) {
        //System.out.print(" i:" + itcnt);
        Spawn_Point();
        closest.stress = 0.0;
        int randt = Logic.wheel.nextInt();
        shave = (100 + (randt % 20));
      }
      this.Shift_Closest();
      planeform.Ping(closest);
      planeform.Plane_Ramp_To_Normal(normal);
      normal.CheckNAN();
      // now calculate the found data point's desire
      Calculate_Point_Desire(closest, desire);
      Pass_Desire_Backward(desire);
    } else {
      //this.closest.loc[ninputs] = 1.0;// if not using the points anymore, all go to red.
    }
    itcnt++;
  }
  /* *************************************************************************************************** */

  @Override
  public void Age_Points() {
    int cnt;
    cnt = plist.size() - 1;
    while (cnt >= 0) {
      PointNd_Mov pm = plist.get(cnt);
      pm.Age();
      if (pm.Is_Expired()) {
        plist.remove(pm);
      }
      cnt--;
    }
  }
  /* *************************************************************************************************** */

  public void Spawn_Point() {
    PointNd_Mov pm = new PointNd_Mov(ndims);
    pm.Copy_From(infire);
    pm.loc[ninputs] = outfire;
    plist.add(pm);
  }
  /* *************************************************************************************************** */

  @Override
  public void Create_Random_Point() {
    PointNd_Mov pm = new PointNd_Mov(ndims);
    pm.Randomize(-1.0, 1.0);
    closest = pm;
    prev_closest = closest;
    plist.add(pm);
  }
  /* *************************************************************************************************** */

  public void Shift_Closest() {
    double us_lrate = 0.9;// 0.3;// here we adapt to upstream influence
    double delta;
    if (true) {
      delta = this.closest.Get_Distance(infire, ninputs);
      this.closest.stress = (this.closest.stress * 0.9) + delta * 0.1;
    }
    for (int dcnt = 0; dcnt < ninputs; dcnt++) {
      delta = this.infire.loc[dcnt] - this.closest.loc[dcnt];
      this.closest.loc[dcnt] += delta * us_lrate;
    }
  }
  /* *************************************************************************************************** */
  double closest_height = 0.0;

  public void Calculate_Point_Desire(PointNd pnt, PointNd pdesire) {
    // now calculate the found data point's desire
    PointNd delta = new PointNd(ndims);// delta is the vector from the point to its place on the plane

    planeform.Attract_Point(pnt, delta);

    PointNd pforce = new PointNd(ndims);
    Calculate_Point_Repulsion(pnt, pforce);
    delta.Add(pforce);

    pdesire.Copy_From(delta);
    // here we want
    //corrlen *= 0.03;// slow down for animation
    double vmove = pdesire.loc[ninputs];
    if (true) {//speed limit
      double slimit = 0.03;// 0.01;
      double sgn = Math.signum(vmove);
      vmove = Math.abs(vmove);
      if (vmove > slimit) {
        vmove = slimit;
      }
      //vmove = slimit;
      vmove *= sgn;
    }
    //double vert_lrate = 0.3;
    double vert_lrate = 1.0;
    Self_Adjust_Height(pnt, vmove * vert_lrate);// snox
  }
  /* *************************************************************************************************** */

  public void Calculate_Point_Repulsion(PointNd pnt, PointNd pforce) {
    // should probably base this on inverse square distance, or inverse power-of-dimensions distance.
    double radius = 0.1;
    radius = 0.4;
    //radius = 0.8;
    radius = 0.6;// works for squares
    //radius = 0.9;
    double factor = 15.0;
    //factor = 30;
    double deltahgt = 1.0;// to weaken repulsion between similar-output points
    //factor *= 10;
    pforce.Clear();
    PointNd delta = new PointNd(ndims);
    int num_pnts = plist.size();
    for (int cnt = 0; cnt < num_pnts; cnt++) {
      PointNd_Mov other = plist.get(cnt);
      pnt.Get_Delta(other, ninputs, delta);
      deltahgt = Math.abs(pnt.loc[ninputs] - other.loc[ninputs]) * 0.5;// max range would be -1 to +1, so divide by 2
      deltahgt -= 0.01;// force FUSING of similar points!
      //deltahgt -= 0.1;// force FUSING of similar points!
      double dist = delta.Magnitude(ninputs);
      if (dist < radius) {
        double force = (radius - dist) / radius;
        force *= deltahgt;
        //force *= Math.pow(other.freshness, 30);
        //force *= other.freshness;
        force /= (double) plist.size();
        //force *= 0.1;// for cubes
        delta.Multiply(-force * factor);
        pforce.Add(delta);
      }
    }
  }
  /* *************************************************************************************************** */

  @Override
  public void Self_Adjust_Height(PointNd pnt, double corr) {
    pnt.loc[ninputs] += corr;
    //System.out.println(" c:" + String.format("%.3f", corr));
  }
  /* *************************************************************************************************** */

  @Override
  public void Pass_Desire_Backward(PointNd bdesire) {
    /* Get all of the tropisms of the closest (calc plane, etc.) then pass all of the desire vector's values backward to our own inlinks.     */
    int num_inlinks = inlinks.size();
    for (int cnt = 0; cnt < num_inlinks; cnt++) {
      In_Link inlink = inlinks.get(cnt);
      inlink.desire = bdesire.loc[cnt];
    }
  }
  /* *************************************************************************************************** */

  public void BP_Net() {
    // start working on traditional bp net here, and move to a whole new project.
  }
}

/*
[1] Let your set of N points be given by x1, x2, …, xN where each x is an N-dimensional column vector.
[2] Form the NxN data matrix X=[x1 x2 … xN]
[3]  The equation of your hyperplane is given by w’*X + b = 0, where w is the hyperplane normal (though not normalized), b is the hyperplane bias (i..e distance from the origin) and w’ means the transpose of w.
[4] We don’t know b but we can arbitrarily set it to 1 (or -1 depending on which way you want w to point) since we will recover the scale when we normalize w.
[5] So all you need to do is solve w=-inverse(X’)*e where e is an N dimensional column vector whose entries are all 1.
[6] Once you have w the hyperplane normal is given by n=w/sqrt(w’*w)

Thus the key step is matrix inversion where you calculate the inverse of X’. If you have more than N points then you should use the pseudo-inverse as this will give you the plane with the best least squares error fit. In terms of efficiency you might want to use the SVD or some other algorithm rather than matrix inversion.

 */
