/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flatnet;

import java.awt.*;
import java.util.ArrayList;

/**
 *
 * @author jcatkeson, February 2009
 */
public class Layers {
  /* Stack for a layered network. */
  public ArrayList<Network> netlist = new ArrayList<Network>();
  public Network inputs,  outputs;
  int itcnt = 0;
  public static boolean fullfire = true;
  /* *************************************************************************************************** */
  public Layers() {
    //Create_0_Layer();
    //Create_2_Layer();
    //this.Create_N_Layer(2);
    //this.Create_N_Layer(3);
    //this.Create_N_Layer(5);
    //this.Create_N_Layer(7, 3);
    //this.Create_N_Layer(3, 3);
    this.Create_N_Layer(5, 2);
  }
  /* *************************************************************************************************** */
  public void Create_N_Layer(int num_layers,int width) {
    inputs = new Network();/* number of layers is number of connection layers */
    Network middle;

    /* inputs doesn't count as a layer, as it cannot learn */
    double xorg = 150, yorg = 50, nodehgt = 120.0;// 220.0;
    inputs.Create_Input_Nodes(2, xorg, yorg);
    netlist.add(inputs);

    for (int lcnt = 1; lcnt < num_layers; lcnt++) {
      middle = new Network();
      middle.Create_Nodes(width, xorg, yorg += nodehgt);
      netlist.add(middle);
    }
    outputs = new Network();
    outputs.Create_Output_Nodes(1, xorg, yorg += nodehgt);
    netlist.add(outputs);

    num_layers++;

    Network net = inputs;
    for (int lcnt = 1; lcnt < num_layers; lcnt++) {
      middle = this.netlist.get(lcnt);
      middle.Connect_From_All(net);
      net = middle;
    }
  //outputs.Connect_From_All(middle); netlist.add(outputs);
  }
  /* *************************************************************************************************** */
  public void Create_0_Layer() {
    inputs = new Network();
    double xorg = 150, yorg = 150, nodehgt = 220.0;
    inputs.Create_Nodes(1, xorg, yorg);
    netlist.add(inputs);
  }
  /* *************************************************************************************************** */
  public void Fire_All() {
    Network net;
    for (int lcnt = 0; lcnt < netlist.size(); lcnt++) {
      net = netlist.get(lcnt);
      net.Collect_And_Fire();
      net.Distribute_Outfire();
    }
  }
  /* *************************************************************************************************** */
  public void Collect_Desire_Backward() {
    for (Network net : netlist) {
      net.Collect_Desire_Backward();
    }
  }
  /* *************************************************************************************************** */
  public void Create_Path(double inval[], double outval) {// for initialization of dystal-style patches
    Network net = netlist.get(0);// first layer only has outfire values, no patches
    Node_Base node = null;
    for (int ncnt = 0; ncnt < inputs.nodelist.size(); ncnt++) {
      node = net.nodelist.get(ncnt);
      node.Set_Outfire(inval[ncnt]);
    }
    net.Distribute_Outfire();
    for (int lcnt = 1; lcnt < netlist.size(); lcnt++) {
      net = netlist.get(lcnt);
      net.Create_Path();
      net.Distribute_Outfire();
    }
    // last we need to read the final outfire, and apply the desired output value.
    // last layer, one node.
    node = outputs.nodelist.get(0);
    node.Set_Outfire(outval);
  }
  public boolean run_lock = false;
  /* *************************************************************************************************** */
  double avgscore = 0.5;
  public void Draw_3d(Graphics gr) {
    Graphics2D g2 = (Graphics2D) gr;
    while (run_lock) {
    }
    run_lock = true;
    int lcnt = 0;
    for (Network net : netlist) {
      net.Draw_3d(g2);
      lcnt++;
    }

    //plot cumulative scoring graphics here.  maybe a bar graph for percentage right.
    g2.setColor(Color.white);
    g2.drawRect(0, 0, (int) 50, 20);
    g2.fillRect(0, 0, (int) (avgscore * 50.0), 20);
    g2.drawString("Iteration:" + Integer.toString(itcnt), 75, 12);
    if (true) {// print stats, iteration count
      /*
      String pntcnt = Integer.toString(itcnt);
      int strhgt = 10;
      g2.setColor(Color.white);
      g2.drawString(pntcnt, xorg, yorg + strhgt);
       */
    }
    // major issues are: either or x or y sensitivity, not both
    // how big should repulsion radius be?
    // de-weight patch points that are no longer used.
    /*
    if (false) {// plot the sigmoid function to see if it looks right
    double xorg = 500, yorg = 500;
    double prevx = -1000.0, prevy = 0.0;
    double xloc = -1.0, yloc = 0.0;
    double prevy2 = prevy, yloc2 = yloc;
    double factor = 50.0;
    g2.setColor(Color.orange);
    g2.drawLine((int) (xorg), (int) (yorg - factor), (int) (xorg), (int) (yorg + factor));
    g2.drawLine((int) (xorg - factor), (int) (yorg), (int) (xorg + factor), (int) (yorg));
    
    g2.drawLine((int) (xorg - 10000), (int) (yorg), (int) (xorg + 10000), (int) (yorg));
    
    //g2.drawLine((int) (xorg - 50.0), (int) (yorg-factor), (int) (xorg + 50.0), (int) (yorg-factor));
    
    for (double cnt = -50.0; cnt < 50.0; cnt += 0.1) {
    //for (double cnt = -0.01; cnt < 0.01; cnt += 0.001) {
    double height = Node.sigmoid(cnt);
    
    xloc = cnt * factor;
    yloc = height * factor;
    g2.setColor(Color.orange);
    g2.drawLine((int) (xorg + prevx), (int) (yorg + prevy), (int) (xorg + xloc), (int) (yorg + yloc));
    //System.out.println("cnt:"+cnt+" dx:" + (xloc - prevx) + " dy:" + (yloc - prevy));
    prevy = yloc;
    
    double height2 = Node.sigmoid_deriv(height);
    yloc2 = height2 * factor;
    g2.setColor(Color.green);
    g2.drawLine((int) (xorg + prevx), (int) (yorg + prevy2), (int) (xorg + xloc), (int) (yorg + yloc2));
    prevy2 = yloc2;
    
    prevx = xloc;
    }
    }
     */
    run_lock = false;
  }
  /* *************************************************************************************************** */
  double desired_out;
  public void Run_Cycle(double inval[], double outval) {
    while (run_lock) {
    }
    run_lock = true;
    int lcnt = 0;
    desired_out = outval;
    {
      Network net = netlist.get(0);// first layer only has outfire values, no patches
      Node_Base node;
      for (int ncnt = 0; ncnt < inputs.nodelist.size(); ncnt++) {
        node = net.nodelist.get(ncnt);
        node.Set_Outfire(inval[ncnt]);
      }
      net.Distribute_Outfire();
      for (lcnt = 1; lcnt < netlist.size(); lcnt++) {
        net = netlist.get(lcnt);
        net.Run_Cycle();
        net.Distribute_Outfire();
      }
      double score = outval * this.outputs.nodelist.get(0).outfire;
      score = Math.signum(score);
      avgscore = avgscore * 0.9 + score * 0.1;
      Apply_Goal(outval);
    }
    if (false) {
      Node_Base output = this.outputs.nodelist.get(0);
      if (((Node) output).plist.size() >= 4) {
        boolean nop = true;
      }
    }
    itcnt++;
    run_lock = false;
  }
  /* *************************************************************************************************** */
  public void Apply_Goal(double goal) {
    // first we need to load the input layer with the inputs
    Node_Base nd = this.outputs.nodelist.get(0);
    if (nd.Learning_Mode) {
      nd.Set_Outfire(goal);
      if (false) {
        double corr = goal - nd.outfire;
        this.outputs.nodelist.get(0).Apply_Desire_To_Closest(corr);
      }
    }
  }
  /* *************************************************************************************************** */
  boolean Learning_Mode_Signal = true;
  public void Set_Learning_Mode(boolean Learning_Mode) {// for initialization of dystal-style patches
    for (Network net : netlist) {
      net.Set_Learning_Mode(Learning_Mode);
    }
  }
  /* *************************************************************************************************** */
}
