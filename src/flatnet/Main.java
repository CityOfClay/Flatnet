package flatnet;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
//http://svchsmvm043/turnkey.jsp
/**
 *
 * @author jcatkeson
 */
public class Main {
  /**
   * @param args the command line arguments
   */
  static Graph_Panel Blob_Panel = new Graph_Panel();
  public static void main(String[] args) {
    // TODO code application logic here
    //Example5Applet Blob_Panel = new Example5Applet();
    JFrame f = new JFrame("Java Graph");
    f.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });

    f.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent ke) {
        int kc = ke.getKeyCode();
        if (kc == java.awt.Event.ESCAPE) {
          System.exit(0);
        } else if (kc == KeyEvent.VK_F) {
          Blob_Panel.fast = !Blob_Panel.fast;
        } else if (kc == KeyEvent.VK_L) {
          Blob_Panel.Learning_Mode_Signal = false;
        }
      }
    });

    f.getContentPane().add("Center", Blob_Panel);
    f.pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int w = 1500;
    int h = 1010;
    f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
    f.setSize(w, h);
    f.show();
    Blob_Panel.start();
  }
}
