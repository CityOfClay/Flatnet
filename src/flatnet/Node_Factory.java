/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flatnet;

import java.util.ArrayList;
import java.awt.*;

/**
 *
 * @author jcatkeson, March 2009
 */
public class Node_Factory {// use this like a namespace
  public class Node_Base {
    /* *************************************************************************************************** */
    int ndims = 3;
    int ninputs = ndims - 1;
    public double outfire = 0.0;
    public int xorg,  yorg;// for plotting
    Bounder Bounds;
    /* *************************************************************************************************** */
    public ArrayList<In_Link> inlinks = new ArrayList<In_Link>();
    public ArrayList<In_Link> outlinks = new ArrayList<In_Link>();
    /* *************************************************************************************************** */
    public Node_Base() {
      Bounds = new Bounder();
      double rad = 50.0;
      Bounds.minmax[0][0] = -rad; //xmin
      Bounds.minmax[0][1] = -rad; //ymin
      Bounds.minmax[0][2] = -rad; //zmin
      Bounds.minmax[1][0] = rad; //xmax
      Bounds.minmax[1][1] = rad; //ymax
      Bounds.minmax[1][2] = rad; //zmax
    }
    /* *************************************************************************************************** */
    public void Run_Cycle() {
    }
    /* *************************************************************************************************** */
    public void Collect_And_Fire() {
    }
    /* *************************************************************************************************** */
    public void Distribute_Outfire() {
      for (In_Link outlink : outlinks) {
        outlink.fireval = outfire;
      }
    }
    /* *************************************************************************************************** */
    public double Collect_Desire_Backward() {
      double avgd = 0.0;
      for (In_Link outlink : outlinks) {
        avgd += outlink.desire;
      }
      avgd /= outlinks.size();
      return avgd;
    }
    /* *************************************************************************************************** */
    public void Set_Outfire(double newoutfire) {
      //System.out.println("Base:" + newoutfire);
      switch (1) {
        case 0:
          newoutfire -= -1.0;
          newoutfire *= this.Bounds.Dep();
          newoutfire += this.Bounds.minmax[0][this.ninputs];
          break;
        case 1:
          newoutfire *= this.Bounds.Dep() / 2;
          break;
        case 2:
          newoutfire *= 20;
          break;
        default:
          break;
      }
      this.outfire = newoutfire;
    }
    /* *************************************************************************************************** */
    public void Add_Inlink(Node_Base upstreamer) {
    }
    /* *************************************************************************************************** */
    public void Create_Path() {// for initialization of dystal-style patches
    }
    /* *************************************************************************************************** */
    public void Draw_3d(Graphics gr) {
      Graphics2D g2 = (Graphics2D) gr;
      float pntband = (float) ((outfire - Bounds.minmax[0][2]) / Bounds.Sz(2));
      float red = pntband, blue = (float) 1.0 - pntband;
      if (blue < 0) {
      } else if (blue > 1.0) {
        blue = 1;
      }
      if (red < 0) {
        red = 0;
      } else if (red > 1.0) {
        red = 1;
      }
      Color pnt_color = new Color(red, 0.0f, blue);
      g2.setColor(pnt_color);
      double rad = 25;
      //g2.fillRect(xorg + (int) Bounds.minmax[0][0], yorg + (int) Bounds.minmax[0][1], (int) Bounds.Wdt(), (int) Bounds.Hgt());
      //g2.fillRect(xorg + (int) -rad, yorg + (int) -rad, (int) rad * 2, (int) rad * 2);
      g2.fillOval(xorg + (int) -rad, yorg + (int) -rad, (int) rad * 2, (int) rad * 2);
    }
    /* *************************************************************************************************** */
    public class Bounder {
      double[][] minmax = new double[2][ndims];
      public double Wdt() {
        return minmax[1][0] - minmax[0][0];
      }
      public double Hgt() {
        return minmax[1][1] - minmax[0][1];
      }
      public double Dep() {
        return minmax[1][2] - minmax[0][2];
      }
      public double Sz(int dim) {
        return minmax[1][dim] - minmax[0][dim];
      }
      public double CtrX() {
        return (minmax[1][0] + minmax[0][0]) / 2.0;
      }
      public double CtrY() {
        return (minmax[1][1] + minmax[0][1]) / 2.0;
      }
      public double CtrZ() {
        return (minmax[1][2] + minmax[0][2]) / 2.0;
      }
      public double Ctr(int dim) {
        return (minmax[1][dim] + minmax[0][dim]) / 2.0;
      }
    }
    /* *************************************************************************************************** */
    public class In_Link {
      public double fireval;
      public double desire;
      public Node_Base upstreamer,  downstreamer;
    }
  }
  public class Node_In extends Node_Base {
  }
  public class Node_Mid extends Node_Base {
    /* *************************************************************************************************** */
    PointNd closest = new PointNd();
    PointNd infire = new PointNd();
    PointNd avg_nd = new PointNd();
    PointNd[] avg_lo = new PointNd[ndims], avg_hi = new PointNd[ndims];
    PointNd[] offctr = new PointNd[2], offctr_cross = new PointNd[2];
    PointNd[] corners;
    double num_trials = 0.0;
    double avg_real_slope = 0.0;
    double avg_test_slope = 0.0;
    PointNd_List plist;
    public Node_Mid() {
      super();
      int num_corners = 1 << ndims;
      corners = new PointNd[num_corners];
      //plist = new PointNd_List(3);
      plist = new PointNd_List(0);
    }
    /* *************************************************************************************************** */
    public class PointNd {
      public int ndims = 3;
      //public ArrayList<double> loc=new ArrayList<double>();
      public double[] loc = null;
      public PointNd() {
        loc = new double[ndims];
        Clear();
      }
      public double getloc(int dim) {
        return loc[dim];
      }
      public void setloc(int dim, double value) {
        loc[dim] = value;
      }
      public void Add(PointNd other) {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] += other.loc[cnt];
        }
      }
      public void Subtract(PointNd other) {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] -= other.loc[cnt];
        }
      }
      public void Copy_From(PointNd other) {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] = other.loc[cnt];
        }
      }
      public void Multiply(double factor) {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] *= factor;
        }
      }
      public double Magnitude(int dimensions) {
        double sumsq = 0.0;
        for (int cnt = 0; cnt < dimensions; cnt++) {
          sumsq += loc[cnt] * loc[cnt];
        }
        return Math.sqrt(sumsq);
      }
      PointNd another;
      public double Get_Distance(PointNd other, int dimensions) {
        another = other;
        double delta, dist = 0.0;// pythagorean distance
        for (int cnt = 0; cnt < dimensions; cnt++) {
          delta = this.loc[cnt] - other.loc[cnt];
          dist += delta * delta;// sum of the squares
        }
        dist = Math.sqrt(dist);
        return dist;
      }
      public double Dot_Product(PointNd other) {
        double retval = 0.0;// assume other is a unit vector.
        for (int cnt = 0; cnt < ndims; cnt++) {
          if ((this.loc[cnt] != 0.0) && (other.loc[cnt] != 0.0)) {// Zero always wins, even against infinity in this usage.
            retval += this.loc[cnt] * other.loc[cnt];
          }
        }
        return retval;
      }
      public void Unitize() { // convert to unit vector
        double length = 0.0;
        for (int cnt = 0; cnt < ndims; cnt++) {
          double axis = this.loc[cnt];
          length += axis * axis;
        }
        length = Math.sqrt(length);//pythagoran length
        if (length == 0.0) {// fudge to avoid divide-by-zero
          length = java.lang.Double.MIN_VALUE;
        }
        for (int cnt = 0; cnt < ndims; cnt++) {
          this.loc[cnt] /= length;
        }
      }
      public void Randomize(double minval, double maxval) {
        double range = maxval - minval;
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] = minval + (Logic.wheel.nextDouble() * range);
        }
      }
      public void Randomize(Bounder bounds) {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] = bounds.minmax[0][cnt] + (Logic.wheel.nextDouble() * bounds.Sz(cnt));
        }
      }
      public void Clear() {
        for (int cnt = 0; cnt < ndims; cnt++) {
          loc[cnt] = 0.0;
        }
      }
      public void CheckNAN() {
        for (int cnt = 0; cnt < ndims; cnt++) {
          if (loc[cnt] != loc[cnt]) {
            boolean noop = true;
          }
        }
      }
      public void CheckInf() {
        for (int cnt = 0; cnt < ndims; cnt++) {
          if (java.lang.Double.isInfinite(loc[cnt])) {
            boolean noop = true;
          }
        }
      }
    }
    /* *************************************************************************************************** */
    public class PointNd_Mov extends PointNd {
      public double[] delta = null;
      public PointNd_Mov() {
        delta = new double[ndims];
        Clear();
      }
    }
    /* *************************************************************************************************** */
    public class PointNd_List extends ArrayList<PointNd_Mov> {
      public PointNd_List(int newsize) {
        xorg = 200;
        yorg = 200;
        double radius = 100;
        double shear = 0.0;
        double zsqueeze = 0.5;
        double zshear = (1.0 - zsqueeze);
        //zshear *= 0.5;
        double boxsz = Bounds.Wdt();
        double ctrx = Bounds.Ctr(0), ctry = Bounds.Ctr(1), ctrz = Bounds.Ctr(2);
        double shearabs = boxsz * shear;
        double shearcorr = boxsz / Math.sqrt((shearabs * shearabs) + (boxsz * boxsz));
        PointNd avg = new PointNd();
        for (int cnt = 0; cnt < newsize; cnt++) {
          PointNd_Mov pnt = new PointNd_Mov();
          pnt.Randomize(Bounds);
          if (true) {// if (Math.sqrt((pnt.loc[0] * pnt.loc[0]) + (pnt.loc[1] * pnt.loc[1])) <= radius) {
            //pnt.loc[0] *= 1.0; pnt.loc[1] *= 1.0;
            double sheardist = pnt.getloc(0) * shear;
            //pnt.loc[1] += sheardist;
            pnt.setloc(2, ((pnt.getloc(2) - ctrz) * zsqueeze) + ctrz);
            // pnt.loc[2] += (pnt.loc[0] - 100.0) * zshear;
            pnt.loc[2] += (pnt.getloc(1) - ctry) * zshear;
            //pnt.loc[2] += ctrz * 0.50;
            //pnt.loc[2] = pnt.loc[0];
            //pnt.loc[0] *= shearcorr;
            //pnt.loc[0] *= 1.0/(Math.sqrt(2.0)+0.2);
            //pnt.loc[1] *= Math.sqrt(2.0);
            pnt.loc[1] += ctry;
            //pnt.loc[1] = pnt.loc[0];
            //pnt.loc[0] = 100+(cnt % 2)*100.0; pnt.loc[1] = 100+(cnt & 2)*100.0;
            avg.Add(pnt);
            this.add(pnt);
          }
        }
        avg.Multiply(newsize);// experiment to move all the points to the centroid
        for (int cnt = 0; cnt < newsize; cnt++) {
          PointNd pnt = new PointNd();
        //pnt.Subtract(avg);
        }
      }
      public void Get_Average(PointNd ret_avg) {
        ret_avg.Clear();
        for (PointNd pnt : this) {
          ret_avg.Add(pnt);
        }
        ret_avg.Multiply(1.0 / (double) this.size());
      }
      /* *************************************************************************************************** */
      public void Get_Lines_Nd(PointNd[] avg_lo, PointNd avg_nd, PointNd[] avg_hi) {
        /* split along the cloud's centroid, and draw lines between centroids of opposite regions  */
        double[] num_avg_lo = new double[ndims],
                num_avg_hi =
                new double[ndims];
        for (int dim = 0; dim < ndims; dim++) {
          avg_lo[dim] = new PointNd();
          avg_hi[dim] = new PointNd();
        }
        for (PointNd pnt : this) {
          for (int dim = 0; dim < ndims; dim++) {
            if (pnt.loc[dim] < avg_nd.loc[dim]) {
              avg_lo[dim].Add(pnt);
              num_avg_lo[dim]++;
            } else if (pnt.loc[dim] > avg_nd.loc[dim]) {// don't really need both, can just weigh against mid-average
              avg_hi[dim].Add(pnt);
              num_avg_hi[dim]++;
            } else {//equal
              boolean noop = true;
            }
          }
        }
        for (int dim = 0; dim < ndims; dim++) {
          avg_lo[dim].Multiply(1.0 / num_avg_lo[dim]);
          avg_hi[dim].Multiply(1.0 / num_avg_hi[dim]);
        }
        for (int cnt = 0; cnt < 2; cnt++) {
        }
        double slope = ((avg_hi[0].loc[1]) - (avg_lo[0].loc[1])) / ((avg_hi[0].loc[0]) - (avg_lo[0].loc[0]));
        avg_test_slope += slope;
      }
      /* *************************************************************************************************** */
//http://answers.yahoo.com/question/index?qid=20070508015302AAXGatb  correlation
      //Correlation(r) = NΣXY - (ΣX)(ΣY) / Sqrt([NΣX2 - (ΣX)2][NΣY2 - (ΣY)2]) 
      //http://mathworld.wolfram.com/LeastSquaresFitting.html
    /* http://www.johndcook.com/blog/2008/10/20/comparing-two-ways-to-fit-a-line-to-data/   */
      public void Get_Slope() {/* http://www.johndcook.com/blog/2008/10/20/comparing-two-ways-to-fit-a-line-to-data/ */
        double sx = 0.0, sy = 0.0, stt = 0.0, sts = 0.0;
        int n = this.size();// this one is really good, but limited to 2 dimensions.
        for (int i = 0; i < n; ++i) {
          PointNd pnt = this.get(i);
          sx += pnt.loc[0];
          sy += pnt.loc[1];
        }
        for (int i = 0; i < n; ++i) {
          PointNd pnt = this.get(i);
          double t = pnt.loc[0] - sx / n;
          stt += t * t;
          sts += t * pnt.loc[1];
        }
        double slope = sts / stt;
        double intercept = (sy - sx * slope) / n;
        avg_real_slope += slope;
      }
      boolean CheckVert(PointNd pnt) {
        boolean flat = true;
        for (int dcnt = 0; dcnt < ninputs; dcnt++) {
          flat &= (pnt.loc[dcnt] == 0.0);
        }
        //flat &= (pnt.loc[ninputs] != 0.0);
        if (flat) {
          boolean noop = true;
        }
        for (int cnt = 0; cnt < ndims; cnt++) {
          if (pnt.loc[cnt] == 1.0) {// orthogonality test
            boolean noop = true;
          }
        }
        return flat;
      }
      /* *************************************************************************************************** */
      boolean use_cutter = false;
      PointNd norm = new PointNd();
      PointNd planeform = new PointNd();
      public void Run_Cycle(PointNd bignorm) {// plist runcycle
        if (this.size() == 0) {
          return;
        }
        this.Get_Average(avg_nd);
        if (use_cutter) {
          this.Get_Cutter_Line();
          this.Divide_Space();
          this.Get_Cross_Product(offctr[1], offctr_cross[1], norm);
          norm.Unitize();
          this.Normal_To_Plane(norm, planeform);
        //this.Get_Plane_Fit(norm, planeform);// from norm to plane
        } else {
          this.Plane_Fitter(planeform, norm);// from plane to norm
        }
        norm.CheckInf();
        boolean vert_norm = CheckVert(norm);
        if (vert_norm) {
          for (int dcnt = 0; dcnt < ndims; dcnt++) {// add jitter to destabilize a perfectly flat plane
            norm.loc[dcnt] += (Logic.wheel.nextDouble() - 0.5) * 1.0;// * java.lang.Double.MIN_VALUE;// 0.000000000000001;
          }
          norm.Unitize();
          this.Normal_To_Plane(norm, planeform);
        }
        norm.CheckNAN();
        bignorm.Copy_From(norm);
        // closest is found by now, so execute the following on closest and put the delta or desired dest in desire.
        if (false) {
          // now calculate each data point's desire
          for (int cnt = 0; cnt < this.size(); cnt++) {
            PointNd_Mov pnt = this.get(cnt);
            //double limit = 999999999.9;
            double limit = 9.0;
            double height = 0.0;
            for (int dim = 0; dim < ninputs; dim++) {
              height += (pnt.loc[dim] - avg_nd.loc[dim]) * (planeform.loc[dim]);// multiply each axis length by the slope for that axis
            }
            height += avg_nd.loc[ninputs];
            PointNd delta = new PointNd();// delta is the vector from the point to its place on the plane
            delta.loc[ninputs] = height - pnt.loc[ninputs];
            double corrlen = delta.Dot_Product(norm);// project delta onto normal, to get straight distance to plane.

            desire.Copy_From(norm);// multiply unit normal by corrector length to get correction vector.
            desire.Multiply(corrlen);

            if (java.lang.Double.isNaN(corrlen)) {
              boolean noop = true;
            }
            corrlen *= 0.03;// slow down for animation
            //corrlen *= 0.00003;// slow down for animation
            //corrlen *= 0.00003;// slow down for animation  worked here
            //corrlen *= 0.001;// worked here
            corrlen *= 0.01;// worked here
            //corrlen *= 0.7;// slow down for animation
            delta.Copy_From(norm);// multiply unit normal by corrector length to get correction vector.
            delta.Multiply(corrlen);
            delta.loc[ninputs] *= 0.0;
            for (int dcnt = 0; dcnt < ndims; dcnt++) {
              pnt.delta[dcnt] = delta.loc[dcnt];
            }
          //pnt.Add(delta);
          }
        }
      }
      /* *************************************************************************************************** */
      public void CheckNAN() {
        for (PointNd pnt : this) {
          pnt.CheckNAN();
        }
      }
      /* *************************************************************************************************** */
      public void Get_Plane_Fit(PointNd normal, PointNd planeform) {// get the formula for the plane that fits our data set
        this.Get_Lines_Nd(avg_lo, avg_nd, avg_hi);
        this.Centralize_Centroids();
        this.Get_Cross_Product(avg_hi[1], avg_hi[0], normal);
        boolean vert = this.CheckVert(norm);
        if (vert) {
          boolean noop = true;
        }
        normal.CheckInf();
        normal.Unitize();// convert normal to unit normal
        this.Normal_To_Plane(normal, planeform);
      }
      /* *************************************************************************************************** */
      public void Plane_Fitter(PointNd normal, PointNd planeform) {
        Best_Fit bf = new Best_Fit();
        double[] plane = new double[ndims + 1];
        Best_Fit.Point3[] points = new Best_Fit.Point3[plist.size()];
        for (int cnt = 0; cnt < plist.size(); cnt++) {
          PointNd pnt = plist.get(cnt);
          Best_Fit.Point3 pnt3 = bf.new Point3(pnt.loc[0], pnt.loc[1], pnt.loc[2]);
          points[cnt] = pnt3;
        }
        bf.getBestFitPlane(points, null, plane);
        for (int cnt = 0; cnt < ndims; cnt++) {
          planeform.loc[cnt] = plane[cnt];
        }
        {
          double test = 0.0;
          for (int tcnt = 0; tcnt < ninputs; tcnt++) {
            test += avg_nd.loc[tcnt] * planeform.loc[tcnt];
          }
          test /= planeform.loc[ninputs];
        }
        //planeform.loc[ninputs] += avg_nd.loc[ninputs];
        //for (int cnt = 0; cnt < ninputs; cnt++) {        planeform.loc[cnt] = plane[cnt] / plane[ninputs];      }
        planeform.Unitize();
        this.Plane_To_Normal(planeform, normal);
        normal.Unitize();// convert normal to unit normal
        boolean nop = true;
      }
      /* *************************************************************************************************** */
      public void Centralize_Centroids() {
        for (int cnt = 0; cnt < ndims; cnt++) {
          for (int dcnt = 0; dcnt < ndims; dcnt++) {
            avg_lo[cnt].loc[dcnt] -= avg_nd.loc[dcnt];
            avg_hi[cnt].loc[dcnt] -= avg_nd.loc[dcnt];
          //offctr[cnt].loc[dcnt]-=avg_nd.loc[dcnt]; offctr_cross[cnt].loc[dcnt]-=avg_nd.loc[dcnt]; don't need this
          }
        }
      }
      /* *************************************************************************************************** */
      double shear0 = 0.0;
      double shear1 = 0.0;
      PointNd offset = new PointNd();// epioffset?
      public void Get_Cutter_Line() {// get the formula for the plane that fits our data set
        //this.Get_Lines_Nd(avg_lo, avg_nd, avg_hi);
        //this.Centralize_Centroids();
        {
          double x = 0.0, y = 0.0;
          double rad = 0.0;
          double ang = 0.0;
          PointNd ctr = new PointNd();
          for (int pcnt = 0; pcnt < plist.size(); pcnt++) {//loop for every point
            PointNd pnt = plist.get(pcnt);
            x = pnt.loc[0] - avg_nd.loc[0];
            y = pnt.loc[1] - avg_nd.loc[1];
            rad = Math.sqrt(x * x + y * y);
            ang = Math.atan2(y, x) * 2.0;
            x = Math.cos(ang) * rad;
            y = Math.sin(ang) * rad;
            ctr.loc[0] += x;
            ctr.loc[1] += y;
          }//end loop
          ctr.Multiply(1.0 / (double) (plist.size()));// get centroid
          rad = ctr.Magnitude(2);
          ang = Math.atan2(ctr.loc[1], ctr.loc[0]) / 2.0;// translate back
          offset.loc[0] = Math.cos(ang) * rad;
          offset.loc[1] = Math.sin(ang) * rad;
        }
      }
      /* *************************************************************************************************** */
      public void Get_Cutter_Line2() {// get the formula for the plane that fits our data set
        PointNd hoffs = new PointNd();
        PointNd angoffs = new PointNd();
        double x = 0.0, y = 0.0;
        double hflip = 0.0, angflipx = 0.0, angflipy = 0.0;
        double rad = 0.0;
        double ang = 0.0;
        PointNd ctr = new PointNd();
        for (int pcnt = 0; pcnt < plist.size(); pcnt++) {//loop for every point
          PointNd pnt = plist.get(pcnt);
          x = pnt.loc[0] - avg_nd.loc[0];
          y = pnt.loc[1] - avg_nd.loc[1];
          // first we average the flipped 
          if (y >= 0) {// flip the top
            if (y != 0 || x > 0) {
              hflip = -x;
            }
          }
          hoffs.loc[0] += hflip;
          hoffs.loc[1] += y;

          // now flip along the angle
          if (y >= x) {
            if (y > x || x > 0) {
              angflipx = -y;
              angflipy = -x;
            }
          }
          angoffs.loc[0] += angflipx;
          angoffs.loc[1] += angflipy;
        }//end loop
        hoffs.Multiply(1.0 / (double) (plist.size()));// get horizontal offset centroid
        angoffs.Multiply(1.0 / (double) (plist.size()));// get angled offset centroid

        x = angoffs.Magnitude(2);// angular offset measures the horizontal/vertical centroid offset 
        y = hoffs.loc[0];// horizontal offset measures the angular centroid offset

        // and what do we do if the offset is on the negative side?
        ang = Math.atan2(y, x) / 2.0;// translate back
        rad = ctr.Magnitude(2);
        offset.loc[0] = Math.cos(ang) * rad;
        offset.loc[1] = Math.sin(ang) * rad;
      }
      /* *************************************************************************************************** */
      public void Divide_Space() {// use cutter line to divide into two subgroups
        for (int cnt = 0; cnt < 2; cnt++) {
          offctr[cnt] = new PointNd();
          offctr_cross[cnt] = new PointNd();
        }
        double[] offcnt = new double[2];
        double[] offcnt_cross = new double[2];
        offcnt[0] = 0.0;
        offcnt[1] = 0.0;
        offcnt_cross[0] = 0.0;
        offcnt_cross[1] = 0.0;
        double x = 0.0, y = 0.0, z = 0.0;
        if (offset.loc[0] == 0.0) {
          boolean nop = true;
        }
        double slope = offset.loc[1] / offset.loc[0];
        double cross_slope = -slope;
        for (int pcnt = 0; pcnt < plist.size(); pcnt++) {//loop for every point
          PointNd pnt = plist.get(pcnt);
          x = pnt.loc[0] - avg_nd.loc[0];
          y = pnt.loc[1] - avg_nd.loc[1];
          z = pnt.loc[2] - avg_nd.loc[2];
          double liney = x * slope;
          // establish which side of the line pnt is on.
          if (liney > y) {
            offctr[0].loc[0] += x;
            offctr[0].loc[1] += y;
            offctr[0].loc[2] += z;
            offcnt[0]++;
          } else if (liney < y) {
            offctr[1].loc[0] += x;
            offctr[1].loc[1] += y;
            offctr[1].loc[2] += z;
            offcnt[1]++;
          } else {// right on the line, do we use it?
          }
          /* and for the cross */
          double cross_linex = y * cross_slope;
          if (cross_linex > x) {
            offctr_cross[0].loc[0] += x;
            offctr_cross[0].loc[1] += y;
            offctr_cross[0].loc[2] += z;
            offcnt_cross[0]++;
          } else if (cross_linex < x) {
            offctr_cross[1].loc[0] += x;
            offctr_cross[1].loc[1] += y;
            offctr_cross[1].loc[2] += z;
            offcnt_cross[1]++;
          } else {// right on the line, do we use it?
          }
        }
        for (int cnt = 0; cnt < 2; cnt++) {
          offctr[cnt].Multiply(1.0 / offcnt[cnt]);
          offctr_cross[cnt].Multiply(1.0 / offcnt_cross[cnt]);
        }
      }
      /* *************************************************************************************************** */
      public void Draw_3d(Graphics g) {
        if (this.size() == 0) {
          return;
        }
        Graphics2D g2 = (Graphics2D) g;
        /* all about the gradient for display */
        PointNd steepest = this.Get_Steepest(norm);
        double[] ratios = new double[ninputs];// the ratios are NOT the inverse slopes.  they are from the steepest line.
        for (int cnt = 0; cnt < ninputs; cnt++) {
          ratios[cnt] = steepest.loc[cnt] / steepest.loc[ninputs];// inverse slope for each shadow of the steepest
        }
        double grad_x0,grad_y0,grad_x1,grad_y1;
        {// why the heck do we need multiple bases?
          double[] bases = new double[ninputs];
          for (int cnt = 0; cnt < ninputs; cnt++) {// base is centroid height minus presumed height for location of centroid
            bases[cnt] = avg_nd.loc[ninputs] - (avg_nd.loc[cnt] / ratios[cnt]);// get the b for z=mx+b
          }
          grad_x0 = (int) ((Bounds.minmax[0][2] - bases[0]) * ratios[0]);// Z=mx+b turned inside out, to get X value at Z ceiling and floor
          grad_y0 = (int) ((Bounds.minmax[0][2] - bases[1]) * ratios[1]);// if y=mx+b, then y-b = mx, and (y-b)/m = x
          grad_x1 = (int) ((Bounds.minmax[1][2] - bases[0]) * ratios[0]);
          grad_y1 = (int) ((Bounds.minmax[1][2] - bases[1]) * ratios[1]);
        }
        Color startColor = new Color(0.0f, 0.0f, 1.0f);//Color startColor = Color.blue;
        Color endColor = new Color(1.0f, 0.0f, 0.0f);//Color endColor = Color.red;
        GradientPaint gradient;
        gradient = new GradientPaint(xorg + (int) grad_x0, yorg + (int) grad_y0, startColor, xorg + (int) grad_x1, yorg + (int) grad_y1, endColor);// A non-cyclic gradient
        g2.setPaint(gradient);
        //g2.fillRect(0, 0, 600, 600);
        g2.fillRect(xorg + (int) Bounds.minmax[0][0], yorg + (int) Bounds.minmax[0][1], (int) Bounds.Wdt(), (int) Bounds.Hgt());

        // now illustrate all the data points.
        int ptsz = 8;
        int halfptsz = ptsz / 2;
        for (int cnt = 0; cnt < this.size(); cnt++) {
          PointNd_Mov pnt = this.get(cnt);
          float pntband = (float) ((pnt.loc[2] - Bounds.minmax[0][2]) / Bounds.Sz(2));

          float red = pntband, blue = (float) 1.0 - pntband;
          if (blue < 0) {
            blue = 0;
          } else if (blue > 1.0) {
            blue = 1;
          }
          if (red < 0) {
            red = 0;
          } else if (red > 1.0) {
            red = 1;
          }
          Color pnt_color = new Color(red, 0.0f, blue);
          g2.setColor(pnt_color);
          g2.fillRect(xorg + (int) pnt.loc[0] - halfptsz, yorg + (int) pnt.loc[1] - halfptsz, ptsz, ptsz);
          g2.setColor(Color.black);
          g2.drawRect(xorg + (int) pnt.loc[0] - halfptsz, yorg + (int) pnt.loc[1] - halfptsz, ptsz, ptsz);
          if (true) {
            g2.setColor(Color.magenta);/* draw the movement line */
            g2.drawLine((int) (xorg + (int) pnt.loc[0]), (int) (yorg + pnt.loc[1]), (int) (xorg + (int) pnt.loc[0] + pnt.delta[0]), (int) (yorg + (int) pnt.loc[1] + pnt.delta[1]));
          }
        }
        if (closest != null) {
          g2.setColor(Color.white);
          g2.drawRect(xorg + (int) closest.loc[0] - halfptsz, yorg + (int) closest.loc[1] - halfptsz, ptsz, ptsz);
        }
        if (use_cutter) {// 
          g2.setColor(Color.white);/* draw the cutter line */
          g2.drawOval((int) (xorg + avg_nd.loc[0]) - 1, (int) (yorg + avg_nd.loc[1]) - 1, 2, 2);
          g2.drawLine((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), (int) (xorg + avg_nd.loc[0] + offset.loc[0]), (int) (yorg + avg_nd.loc[1] + offset.loc[1]));

          g2.setColor(Color.magenta);
          try {
            g2.drawLine((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), (int) (xorg + avg_nd.loc[0] + offctr[0].loc[0]), (int) (yorg + avg_nd.loc[1] + offctr[0].loc[1]));
          } catch (Throwable e) {
            boolean nop = true;
          }
          g2.setColor(Color.green);
          g2.drawLine((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), (int) (xorg + avg_nd.loc[0] + offctr_cross[0].loc[0]), (int) (yorg + avg_nd.loc[1] + offctr_cross[0].loc[1]));
        }
        if (false) {
          shear0 = avg_hi[1].loc[0];/* x shear */
          shear1 = avg_hi[0].loc[1];/* y shear */
          if (shear0 >= 0.0 && shear1 >= 0.0) {
            g2.setColor(Color.white);/* draw the cutter line */
            g2.drawOval((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), 2, 2);
            g2.drawLine((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), (int) (xorg + avg_nd.loc[0] + shear0), (int) (yorg + avg_nd.loc[1] + shear1));
          } else {
            g2.setColor(Color.green);/* draw the cutter line */
            g2.drawOval((int) (xorg + avg_nd.loc[0]), (int) (yorg + avg_nd.loc[1]), 2, 2);
            g2.drawLine((int) (xorg + avg_nd.loc[0]), (int) (yorg + +avg_nd.loc[1]), (int) (xorg + avg_nd.loc[0] + shear0), (int) (yorg + avg_nd.loc[1] - shear1));
          }
        }
      }
      /* *************************************************************************************************** */
      public double[] Get_Cross_Product(double[] a, double[] b) {
        double[] p = new double[3];
        p[0] = (a[1] * b[2] - a[2] * b[1]);
        p[1] = (a[2] * b[0] - a[0] * b[2]);
        p[2] = (a[0] * b[1] - a[1] * b[0]);
        return p;
      }
      /* *************************************************************************************************** */
      public void Get_Cross_Product(PointNd a, PointNd b, PointNd normal) {
        normal.Clear();
        normal.loc[0] = (a.loc[1] * b.loc[2] - a.loc[2] * b.loc[1]);
        normal.loc[1] = (a.loc[2] * b.loc[0] - a.loc[0] * b.loc[2]);
        normal.loc[2] = (a.loc[0] * b.loc[1] - a.loc[1] * b.loc[0]);
      }
      public PointNd Get_Steepest(PointNd norm) {// get the steepest line on a plane with respect to Z by rotating its normal 90 degrees.
        PointNd steep = new PointNd();
        double vertical = norm.loc[ninputs]; // last dimension is the output 'Z' dim.
        double floorpotenuse = 0.0;// first get the floorpotenuse
        for (int cnt = 0; cnt < ninputs; cnt++) {
          double axis = norm.loc[cnt];
          floorpotenuse += axis * axis;
        }
        floorpotenuse = Math.sqrt(floorpotenuse);
        steep.loc[ninputs] = floorpotenuse;// the result 'Z' value is the normal's floorpotenuse
        for (int cnt = 0; cnt < ninputs; cnt++) {
          double axis = norm.loc[cnt];
          double ratio = (axis / floorpotenuse);
          steep.loc[cnt] = -vertical * ratio;// will always point up if normal points up.
        }
        return steep;
      }
      public void Normal_To_Plane(PointNd norm, PointNd plane) {// take the normal, and get the formula of the plane (x y z), with respect to z (or last dimension)
        double height = norm.loc[ninputs];
        if (height == 0.0) {
          height = java.lang.Double.MIN_VALUE;
        }
        for (int dimcnt = 0; dimcnt < ninputs; dimcnt++) {
          plane.loc[dimcnt] = (-norm.loc[dimcnt] / height);// multiply each axis length by the slope for that axis
        }
      }
      public void Plane_To_Normal(PointNd plane, PointNd norm) {// take the normal, and get the formula of the plane (x y z), with respect to z (or last dimension)
        double height = 1.0;
        for (int dimcnt = 0; dimcnt < ndims; dimcnt++) {
          norm.loc[dimcnt] = (-plane.loc[dimcnt] * height);
        }
      }
    }/* PointNd_List */
    /* *************************************************************************************************** */

    @Override
    public void Draw_3d(Graphics gr) {
      plist.Draw_3d(gr);
    }
    /* *************************************************************************************************** */
    public void Increase_Dims() {
      //this.ndims++;
    }
    /* *************************************************************************************************** */
    @Override
    public void Add_Inlink(Node_Base upstreamer) {
      In_Link inlink = new In_Link();
      Node_Base usnode = null;
      try {
        usnode = (Node_Base) upstreamer;
      } catch (Throwable e) {
        boolean nop = true;
      }
      inlink.upstreamer = usnode;
      inlink.downstreamer = this;
      usnode.outlinks.add(inlink);
      this.inlinks.add(inlink);
      this.Increase_Dims();
    }
    /* *************************************************************************************************** */
    public void Load_Infires(PointNd infire) {
      int ndims_local = inlinks.size();// this.ndims;
      for (int cnt = 0; cnt < ndims_local; cnt++) {
        // we want to go through each inlink, and save its values in a point's dimension
        In_Link inlink = inlinks.get(cnt);
        infire.loc[cnt] = inlink.fireval;
      }
    }
    /* *************************************************************************************************** */
    public PointNd Find_Closest(int dimensions) {
      PointNd closest_ptr = null;
      Load_Infires(infire);
      double dist = 0.0, mindist = java.lang.Double.POSITIVE_INFINITY;
      int pcnt = 0;
      if (true) {
        for (pcnt = 0; pcnt < this.plist.size(); pcnt++) {
          PointNd pnt = this.plist.get(pcnt);
          dist = infire.Get_Distance(pnt, dimensions);
          if (dist == java.lang.Double.POSITIVE_INFINITY) {
            boolean nop = true;
          }
          if (dist < mindist) {
            mindist = dist;
            closest_ptr = pnt;
          }
        }
      }
      if (closest_ptr == null) {
        boolean noop = true;
      }
      return closest_ptr;
    }
    /* *************************************************************************************************** */
    @Override
    public void Collect_And_Fire() {
      closest = this.Find_Closest(ninputs);
      outfire = closest.loc[ninputs];
    }
    /* *************************************************************************************************** */
    @Override
    public void Distribute_Outfire() {
      for (In_Link outlink : outlinks) {
        outlink.fireval = outfire;
      }
    }
    /* *************************************************************************************************** */
    @Override
    public double Collect_Desire_Backward() {
      double lrate = 0.03;
      double avgd = 0.0;
      if (outlinks.size() > 0) {
        for (In_Link outlink : outlinks) {
          avgd += outlink.desire;
        }
        avgd /= outlinks.size();
      }
      double newval = this.closest.loc[ninputs];
      double weight = 1.0;//sigmoid_deriv(newval / 50.0);
      newval = newval + avgd * lrate * weight;
      if (newval < Bounds.minmax[0][ninputs]) {
        newval = Bounds.minmax[0][ninputs];
      } else if (newval > Bounds.minmax[1][ninputs]) {
        newval = Bounds.minmax[1][ninputs];
      }
      this.closest.loc[ninputs] = newval;
      return avgd;
    }
    /* *************************************************************************************************** */
    double sigmoid(double xin) {/* symmetrical sigmoid function in range -1.0 to 1.0. */
      double factor = 1.0;//100.0;
      double OutVal;
      if (xin < -30.0 * factor) {
        OutVal = (0.0);  /* -30 used to prevent overflow */
      } else {
        if (xin > 30.0 * factor) {
          OutVal = (1.0);  /* 30 used to prevent overflow */
        } else {
          OutVal = (1.0 / (1.0 + Math.exp(-xin)));/* sigmoid curve right here */
        // OutVal = (1.0/(1.0+exp(-xin)));/* sigmoid curve right here */
        /* basically e to the (-x) slopes downward from infinity and approaches zero.  So we add one and it goes from infinity down to approach 1.
        So, 1.0 divided by that slope gives a curve from 1/infinity to 1/1, or 0.0 to 1.0 */
        /* it might be simpler to replace this with arctangent, though the output will be in degrees from straight down to straight up */
        }
      }
      OutVal = (OutVal * 2.0) - 1.0; // make symmetric
      return (OutVal);
    }
    /* *************************************************************************************************** */
    double sigmoid_deriv(double Value) { /* modified for default return  -dbr */
      /* Given the unit's activation value and sum of weighted inputs,
       *  compute the derivative of the activation with respect to the sum.
       *  Defined types are SIGMOID (-1 to +1) and ASYMSIGMOID (0 to +1).
       * */
      Value = (Value + 1.0) / 2;
//    Value = (Value * 0.5)+0.5;
      double SigmoidPrimeOffset = 0.1;
      /*  asymmetrical sigmoid function in range 0.0 to 1.0.  */
      double returnval = (SigmoidPrimeOffset + (Value * (1.0 - Value)));
      return returnval * 1.0;
    //return ((SigmoidPrimeOffset + (Value * (1.0 - Value))));
    }
    /* *************************************************************************************************** */
    @Override
    public void Set_Outfire(double newoutfire) {
      //System.out.println("Node:" + newoutfire);
      if (false) {
        if (true) {
          newoutfire -= -1.0;
          newoutfire *= this.Bounds.Dep();
          newoutfire += this.Bounds.minmax[0][this.ninputs];
        } else {
          newoutfire *= this.Bounds.Dep() / 2;
        }
      } else {
        super.Set_Outfire(newoutfire);
      }
      this.closest.loc[ninputs] = this.outfire;
    }
    /* *************************************************************************************************** */
    @Override
    public void Create_Path() {// for initialization of dystal-style patches
      PointNd_Mov infire = new PointNd_Mov();
      Load_Infires(infire);
      infire.loc[ninputs] = Bounds.minmax[0][ninputs] + Logic.wheel.nextDouble() * Bounds.Sz(ninputs);// we need to expand this to the size of the plot box.
      this.plist.add(infire);
      closest = infire;
      outfire = closest.loc[ninputs];
    }
    /* *************************************************************************************************** */
    PointNd planeform = new PointNd();
    PointNd normal = new PointNd();
    PointNd desire = new PointNd();
    /* *************************************************************************************************** */
    @Override
    public void Run_Cycle() {
      this.Collect_And_Fire();// this one is messing things up
      this.Shift_Closest();
      plist.Run_Cycle(normal);
      //plist.Get_Plane(planeform, normal);
      // now calculate the found data point's desire
      Calculate_Point_Desire(closest, desire);
      Pass_Desire_Backward();
    }
    public void Shift_Closest() {
      double lrate = 0.3;
      for (int dcnt = 0; dcnt < ninputs; dcnt++) {
        double delta = this.infire.loc[dcnt] - this.closest.loc[dcnt];
        this.closest.loc[dcnt] += delta * lrate;
      }
    }
    /* *************************************************************************************************** */
    public void Calculate_Point_Desire(PointNd pnt, PointNd pdesire) {
      // now calculate the found data point's desire
      double lrate = 0.05;
      double height = 0.0;
      for (int dim = 0; dim < ninputs; dim++) {
        height += (pnt.loc[dim] - avg_nd.loc[dim]) * (planeform.loc[dim]);// multiply each axis length by the slope for that axis
      }
      height += avg_nd.loc[ninputs];// add in the base offset
      PointNd delta = new PointNd();// delta is the vector from the point to its place on the plane
      delta.loc[ninputs] = height - pnt.loc[ninputs];
      double corrlen = delta.Dot_Product(normal);// project delta onto normal, to get straight distance to plane.
      //corrlen *= 0.03;// slow down for animation/gradient descent
      delta.Copy_From(normal);// multiply unit normal by corrector length to get correction vector.
      delta.Multiply(corrlen);
      //delta.loc[ninputs] *= 0.0;
      pdesire.Copy_From(delta);
      //corrlen *= 0.03;// slow down for animation
      //pnt.loc[ninputs] += corrlen * lrate;
      if (java.lang.Double.isNaN(corrlen)) {
        boolean noop = true;
      }
    }
    /* *************************************************************************************************** */
    public void Pass_Desire_Backward() {
      /* Get all of the tropisms of the closest (calc plane, etc.) then pass all of the desire vector's values backward to our own inlinks.     */
      int num_inlinks = inlinks.size();
      for (int cnt = 0; cnt < num_inlinks; cnt++) {
        In_Link inlink = inlinks.get(cnt);
        inlink.desire = desire.loc[cnt];
      //System.out.println("desire:" + inlink.desire);
      }
    }
  }
  public class Node_Out extends Node_Mid {
  }
}
