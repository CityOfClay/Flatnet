package flatnet;

/**
 * 
 * @author jcatkeson
 */
import java.util.Vector;
import java.awt.*;
import javax.swing.*;

public class Graph_Panel extends JPanel implements Runnable {

  private Thread thread;
  Vector lines = new Vector();
  public boolean fast = false;
  private Font font12 = new Font("serif", Font.PLAIN, 12);
  Color jfcBlue = new Color(204, 204, 255);
  Color pink = new Color(255, 175, 175);

  interface ConstantStuff {

    public static final int maxdims = 2;
    public static final boolean SillyPlatform = true;
  }
  double num_trials = 0.0;
  double avg_real_slope = 0.0;
  double avg_test_slope = 0.0;
  int ndims = 3;//int ninputs = ndims-1;
  Layers lay;
  public boolean gridding = false;
  public boolean netting = false;
  public Grid grd = null;
  public Network net = null;
  /* *************************************************************************************************** */

  public Graph_Panel() {
    setBackground(new Color(20, 20, 20));
    onShutdown();
    if (gridding) {
      grd = new Grid();
      grd.xorg = 20;
      grd.yorg = 30;
    } else if (netting) {
      net = new Network();
      //net.Create_Circle(12, 100, 100);
      //net.Create_Hypercube(2, 100, 100);
      //net.Create_Hypercube(3, 100, 100);
      //net.Create_Hypercube(4, 100, 100);
      net.Create_All_To_All(4, 100, 100);
    } else {
      lay = new Layers();
      Init_Layers();
    }
    num_trials++;
  }
  /* *************************************************************************************************** */

  public static void onShutdown() {
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
      @Override
      public void run() {
        //Do the onShutdown stuff here.
        boolean nop = true;
      }
    }));
  }
  /* *************************************************************************************************** */
  public static double infactor = 0.7;//0.999;// 
  public static double outfactor = 0.7;//0.999;// 

  public void Init_Layers() {
    double[] invec = new double[2];
    double outval = 0.0;
    for (int tcnt = 0; tcnt < 0; tcnt++) {
      Logic.Bit_To(tcnt, 0, 1, invec);
      boolean result = Training_Function(tcnt, 2);
      outval = result ? 1.0 : -1.0;
      outval *= outfactor;
      lay.Create_Path(invec, outval);
    }
    // create random paths
    for (int tcnt = 0; tcnt < 1; tcnt++) {
      Create_Random_Path();
    }
  }
  /* *************************************************************************************************** */

  public void Create_Random_Path() {
    double[] invec = new double[2];
    double outval = 0.0;
    invec[0] = Logic.wheel.nextDouble() * 2.0 - 1.0;
    invec[1] = Logic.wheel.nextDouble() * 2.0 - 1.0;
    outval = Logic.wheel.nextDouble() * 2.0 - 1.0;
    lay.Create_Path(invec, outval);
  }
  /* *************************************************************************************************** */

  @Override
  public void paint(Graphics g) {
    Dimension d = getSize();
    int w = d.width;
    int h = d.height;
    Graphics2D g2 = (Graphics2D) g;
    g2.setBackground(getBackground());
    g2.clearRect(0, 0, w, h);
    g2.setColor(Color.white);

    //String itstr = String.format("%###,###", itcnt);
    /*
    int lower = itcnt % 1000;
    int higher = itcnt - lower;
    g2.drawString("Iteration:" + Integer.toString(higher) + "," + Integer.toString(lower), 75, 12);
     */
    g2.drawString("Iteration:" + Integer.toString(itcnt), 75, 12);
    if (gridding) {
      grd.Display(g2);
    } else {
      boolean lmode = true;
      if (netting) {
        lmode = net.nodelist.get(0).Learning_Mode;
        net.Draw_3d(g2);

        double xorg = 10;
        double yorg = 800;
        double prevx = xorg;
        double nextx = xorg;
        double nexty = yorg;
        double prevy = yorg;
        g2.setColor(Color.green);
        for (int cnt = 0; cnt < tracemark; cnt++) {
          prevy = nexty;
          nexty = yorg + -trace[cnt] * 20.0;
          g2.drawLine((int) prevx, (int) prevy, (int) nextx, (int) nexty);
          prevx = nextx;
          nextx = xorg + cnt * 3.0;
        }

      } else {
        lmode = lay.netlist.get(0).nodelist.get(0).Learning_Mode;
        lay.Draw_3d(g2);
      }
      if (lmode) {
        g2.setColor(Color.white);
      } else {
        g2.setColor(Color.pink);
      }
      g2.drawString("Learning:" + lmode, 200, 12);
    }
    num_trials++;
  }
  /* *************************************************************************************************** */

  public void start() {
    thread = new Thread(this);
    thread.setName("SamplingGraph");
    thread.start();
  }
  /* *************************************************************************************************** */

  public void stop() {
    if (thread != null) {
      thread.interrupt();
    }
    thread = null;
  }
  /* *************************************************************************************************** */

  @Override
  public void run() {
    while (thread != null) {
      if (gridding) {
        this.Run_Grid();
      } else if (netting) {
        this.Run_Network();
      } else {
        this.Run_Layers();
      }
    }
  }
  /* *************************************************************************************************** */
  int tracelen = 400;
  int tracemark = 0;
  double[] trace = new double[tracelen];

  public void Run_Network() {
    int delay = 500;
    if (fast) {
      delay = 0;
    }
    if (!Learning_Mode_Signal) {
      net.Set_Learning_Mode(false);
      Learning_Mode_Signal = true;
    }
    double pulse;
    int bits = itcnt % 7;
    if (bits == 0) {
      pulse = 1.0;
    } else {
      //pulse = 0.0;
      pulse = -1.0;
    }
    tracemark = itcnt % tracelen;
    double outval = net.Full_Run_Cycle(pulse);
    trace[tracemark] = outval;
    repaint();
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
    }
    itcnt++;
  }
  /* *************************************************************************************************** */

  public void Run_Grid() {
    int delay = 50;
    if (fast) {
      delay = 0;
    }
    grd.snoi();
    //grd.Randomize();
    repaint();
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
    }
  }
  /* *************************************************************************************************** */

  public boolean Training_Function(long bits, int bitlen) {
    switch (2) {
      case 0:
        return (bits & 1) == 1;// pass through
      case 1:
        return Logic.Multi_Xor(bits, bitlen);
      case 2:
        return Logic.Multi_And(bits, bitlen);
      case 3:
        return Logic.Multi_Or(bits);
      default:
        return false;
    }
  }
  boolean Learning_Mode_Signal = true;
  /* *************************************************************************************************** */
  int itcnt = 1;

  public void Run_Layers() {
    int delay = 1000;
    fast=true;
    if (fast) {
      delay = 0;
    }
    if (!Learning_Mode_Signal) {
      lay.Set_Learning_Mode(false);
      Learning_Mode_Signal = true;
    }
    double fuzz = 0.0;// 0.5;
    //fuzz = 0.4;
    double fuzzcomp = 1.0 - fuzz;
    double[] invec = new double[2];
    double outval = 0.0;
    for (int tcnt = 0; tcnt < 4; tcnt++) {
      infactor = (Logic.wheel.nextDouble() * fuzz) + fuzzcomp;
      //outfactor = infactor;
      if (false) {
        if (!lay.outputs.nodelist.get(0).Learning_Mode) {
          infactor = 1.0;
          outfactor = 0.7;
        }
      }
      Logic.Bit_To(tcnt, 0, 1, invec);
      for (int bcnt = 0; bcnt < 2; bcnt++) {
        infactor = (Logic.wheel.nextDouble() * fuzz) + fuzzcomp;
        invec[bcnt] *= infactor;
      }
      //invec[1] = (Logic.wheel.nextDouble() * 2.0) - 1.0;// pass through
      boolean result = Training_Function(tcnt, 2);
      outval = result ? 1.0 : -1.0;
      outval *= outfactor;
      lay.Run_Cycle(invec, outval);
      itcnt++;
      if (false) {
        lay.Collect_Desire_Backward();
      }
      try {
        Thread.sleep(delay);
      } catch (InterruptedException e) {
      }
      repaint();
    }
  }
  /* *************************************************************************************************** */

  void delete(Object doomed) {
  }
  /* *************************************************************************************************** */
  /*
   * fun network distance alg, not right yet:
   *
   * set all node distances to -1;
   *
   * distance = 0;
   * get first node, label 0, add to queue
   * distance ++;
   * while queue.length>0 {
   *   pop a node from queue[cnt];// whatever the web crawler logic was
   *   foreach thing this node connects down stream to {
   *     if that ds thing's distance == -1{// only go for uninitialized nodes
   *       set that ds thing's distance = distance;
   *       add that ds thing to the queue;
   *     }
   *   }
   *   distance ++;
   * }
   */
} // End class Graph_Panel

