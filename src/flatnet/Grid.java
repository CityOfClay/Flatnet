/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flatnet;

import java.awt.*;
import java.util.ArrayList;

/**
 *
 * @author JCAtkeson
 * http://www.ai-junkie.com/ann/som/som1.html
 */
/* *************************************************************************************************** */
public class Grid {
  public int ndims = 3;
  public int ninputs = ndims - 1;
  public class Cell {
    public double hotness;
    Node_Base.PointNd pnt;
    Node_Base.PointNd topopnt;
    public int scrwdt = 5,  scrhgt = 5;
    /* *************************************************************************************************** */
    public Cell() {
      pnt = new Node_Base.PointNd(ndims);
      topopnt = new Node_Base.PointNd(ndims);
    }
    /* *************************************************************************************************** */
    public double height() {
      return this.pnt.loc[ninputs];
    }
    /* *************************************************************************************************** */
    public void Set_Height(double newhgt) {
      this.pnt.loc[ninputs] = newhgt;
    }
    /* *************************************************************************************************** */
    public void Display(Graphics2D g2, int xloc, int yloc) {
      // work out height color. 
      float pntband = (float) ((height() + 1.0) / 2.0);// map to range 0.0 - 1.0

      float red = pntband, blue = (float) 1.0 - pntband;
      if (blue < 0) {
        blue = 0;
      } else if (blue > 1.0) {
        blue = 1;
      }
      if (red < 0) {
        red = 0;
      } else if (red > 1.0) {
        red = 1;
      }
      Color pnt_color = new Color(red, 0.0f, blue);
      g2.setColor(pnt_color);
      g2.fillRect(xloc, yloc, scrwdt, scrhgt);
      g2.setColor(Color.black);
      g2.drawRect(xloc, yloc, scrwdt, scrhgt);
    }
  }
  ArrayList cols = new ArrayList();
  public Cell[][] cells;
  int rez = 50;
  int numcols = rez;
  int numrows = rez;
  int scrwdt = 300;
  int scrhgt = 300;
  public int colwdt,  rowhgt;
  public int xorg,  yorg;
  /* *************************************************************************************************** */
  public Grid() {
    xorg = 0;
    yorg = 0;
    colwdt = scrwdt / numcols;
    rowhgt = scrhgt / numrows;
    cells = new Cell[numrows][numcols];
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = new Cell();
        cl.scrwdt = colwdt;
        cl.scrhgt = rowhgt;
        double xloc = 2.0 * (xdex / (double) numcols) - 1.0;
        double yloc = 2.0 * (ydex / (double) numrows) - 1.0;
        cl.topopnt.loc[0] = xloc;
        cl.topopnt.loc[1] = yloc;
        cells[ydex][xdex] = cl;
      }
    }
    Randomize();
  }
  /* *************************************************************************************************** */
  public void Display(Graphics2D g2) {
    for (int ydex = 0; ydex < numrows; ydex++) {
      int yloc = yorg + ydex * rowhgt;
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        int xloc = xorg + xdex * colwdt;
        cl.Display(g2, xloc, yloc);
      }
    }
    Display_Mesh(g2);
  }
  /* *************************************************************************************************** */
  public class scrloc {
    public double xloc,  yloc;
  }
  /* *************************************************************************************************** */
  public void Display_Mesh(Graphics2D g2) {
    /*
    Node_Base.PointNd yprev[] = new Node_Base.PointNd[numcols];
    for (int cnt = 0; cnt < numcols; cnt++) {
    Node_Base.PointNd col = new Node_Base.PointNd(ndims);
    yprev[cnt] = col;
    }
     */
    scrloc xyup = new scrloc();
    scrloc xyleft = new scrloc();
    scrloc xynow = new scrloc();

    int ydexprev = 0;
    for (int ydex = 0; ydex < numrows; ydex++) {
      int xdexprev = 0;
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        Project(cl.pnt, xynow);
        Cell upcl = cells[ydexprev][xdex];
        Project(upcl.pnt, xyup);
        Cell leftcl = cells[ydex][xdexprev];
        Project(leftcl.pnt, xyleft);

        g2.setColor(Color.white);
        g2.drawLine((int) xyleft.xloc, (int) xyleft.yloc, (int) xynow.xloc, (int) xynow.yloc);
        g2.drawLine((int) xyup.xloc, (int) xyup.yloc, (int) xynow.xloc, (int) xynow.yloc);

        xdexprev = xdex;
      }
      ydexprev = ydex;
    }
  }
  /* *************************************************************************************************** */
  public void Project(Node_Base.PointNd pnt, scrloc xy) {
    xy.xloc = xorg + ((1.0 + pnt.loc[0]) / 2.0) * scrwdt;
    xy.yloc = yorg + (scrhgt) + ((1.0 + pnt.loc[1]) / 2.0) * scrhgt;
  }
  /* *************************************************************************************************** */
  public void Iterate() {
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
      }
    }
  }
  /* *************************************************************************************************** */
  public void Randomize() {
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        cl.Set_Height(-1.0 + (Logic.wheel.nextDouble() * 2.0));
        //cl.pnt.Randomize(-1.0, 1.0);
        double xloc = xdex / (double) numcols;
        double yloc = ydex / (double) numrows;
        cl.pnt.loc[0] = (xloc * 2.0) - 1.0;
        cl.pnt.loc[1] = (yloc * 2.0) - 1.0;
        cl.pnt.Jitter(-0.002, 0.002);
        cl.pnt.Randomize(-1.0, 1.0);
      }
    }
  }
  /* *************************************************************************************************** */
  public void Sprinkle_Random_Heights() {
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        if (Logic.wheel.nextInt() % 10000 == 0) {
          cl.Set_Height(-1.0 + (Logic.wheel.nextDouble() * 2.0));
        }
      }
    }
  }
  /* *************************************************************************************************** */
  public void snoo() {
    int[][] data;
    data = new int[10][];
    data[0] = new int[5];
    data[1] = new int[20];
  }
  /* *************************************************************************************************** */
  public double Get_Distance(Node_Base.PointNd pnt, double xloc, double yloc, int dimensions) {
    double delta, dist = 0.0;// pythagorean distance
    {
      delta = xloc - pnt.loc[0];
      dist += delta * delta;// sum of the squares
      delta = yloc - pnt.loc[1];
      dist += delta * delta;// sum of the squares
    }
    dist = Math.sqrt(dist);
    return dist;
  }
  /* *************************************************************************************************** */
  public Cell Get_BMU(Node_Base.PointNd pnt) {
    Cell bmu = null;
    double dist;
    double mindist = java.lang.Double.POSITIVE_INFINITY;
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        dist = pnt.Get_Distance(cl.pnt, ninputs);
        if (mindist > dist) {
          mindist = dist;
          bmu = cl;
        }
      }
    }
    return bmu;
  }
  /* *************************************************************************************************** */
  public Cell Get_Topo_BMU(Node_Base.PointNd pnt) {
    Cell bmu = null;
    double dist;
    double mindist = java.lang.Double.POSITIVE_INFINITY;
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        dist = pnt.Get_Distance(cl.topopnt, ninputs);
        if (mindist > dist) {
          mindist = dist;
          bmu = cl;
        }
      }
    }
    return bmu;
  }
  /* *************************************************************************************************** */
  public void snoi() {
    Node_Base.PointNd randpnt = new Node_Base.PointNd(ndims);
    randpnt.Randomize(-1.0, 1.0);
    if (false) {
      double radius = (Logic.wheel.nextDouble() * 1.0);
      double angle = (Logic.wheel.nextDouble() * Math.PI * 2.0);
      double xloc = Math.cos(angle) * radius;
      double yloc = Math.sin(angle) * radius;
      randpnt.loc[0] = xloc;
      randpnt.loc[1] = yloc;
    }
    //this.Ping(randpnt);
    this.Ping2(randpnt);
    this.Sprinkle_Random_Heights();
  }
  /* *************************************************************************************************** */
  public void Ping2(Node_Base.PointNd pnt) {
    // hit the grid with one point and adjust all cells accordingly.
    Cell bmu;
    //bmu = Get_BMU(pnt);
    bmu = Get_Topo_BMU(pnt);
    double bmu_hgt = bmu.height();
    pnt.loc[ninputs] = bmu_hgt;

    double bzone = 0.17;
    double merad = 0.0;// 0.0001;
    double effectrad = merad + bzone;

    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        Cell cl = cells[ydex][xdex];
        // get TOPOLOGICAL distance from hit point.
        double radius;
        radius = bmu.topopnt.Get_Distance(cl.topopnt, ninputs);
        //radius = pnt.Get_Distance(cl.topopnt, ninputs);

        if (radius <= effectrad) {
          cl.Set_Height(bmu_hgt);
        }
      }
    }
  }
  /* *************************************************************************************************** */
  double zone = 1.0;
  public void Ping(Node_Base.PointNd pnt) {
    // hit the grid with one point and adjust all cells accordingly.
    Cell bmu = Get_BMU(pnt);
    //pnt.Copy_From(bmu.pnt);
    pnt.loc[ninputs] = bmu.pnt.loc[ninputs];
    double merad = 0.0;// 0.0001;
    double effectrad = merad + zone;
    double lrate = 0.01;
    //lrate = 0.9;

    Node_Base.PointNd temp = new Node_Base.PointNd(ndims);
    Node_Base.PointNd corr = new Node_Base.PointNd(ndims);
    for (int ydex = 0; ydex < numrows; ydex++) {
      for (int xdex = 0; xdex < numcols; xdex++) {
        double influence = 0;
        double outside = 0;
        Cell cl = cells[ydex][xdex];
        // get TOPOLOGICAL distance from hit point.
        double radius = bmu.topopnt.Get_Distance(cl.topopnt, ninputs);

        if (radius <= effectrad) {
          if (radius <= merad) {
            influence = 1.0 * lrate;
          } else {
            double delta = radius - merad;

            corr.Copy_From(pnt);
            corr.Subtract(cl.pnt);
            influence = Math.exp(-(delta * delta) / (2 * zone * zone));
            //influence = Math.exp(-(delta * delta * delta) / (2 * zone * zone * zone));
            //influence = delta / zone;
            influence = 1.0;// http://www.generation5.org/content/1999/selforganize.asp
            corr.Multiply(lrate * influence);
            cl.pnt.Add(corr);

          //delta = Math.sqrt(delta);
          //influence = (1.0 - (delta / zone)) * lrate;
          //influence = influence * influence;// * influence;
          }
          if (false) {
            outside = (1.0 - influence);
            // now influence is the weight to apply to the pnt side, and outside is the weight to apply to the environment.
            temp.Copy_From(pnt);
            temp.Multiply(influence);
            cl.pnt.Multiply(outside);
            cl.pnt.Add(temp);
          }
        }
      }
    }
    zone *= 0.9999995;
  }
}
/*
//enter the training loop
if (--m_iNumIterations > 0)
{
//the input vectors are presented to the network at random
int ThisVector = RandInt(0, data.size()-1);

//present the vector to each node and determine the BMU
m_pWinningNode = FindBestMatchingNode(data[ThisVector]);

//calculate the width of the neighbourhood for this timestep
m_dNeighbourhoodRadius = m_dMapRadius * exp(-(double)m_iIterationCount/m_dTimeConstant);

//Now to adjust the weight vector of the BMU and its
//neighbours

//For each node calculate the m_dInfluence (Theta from equation 6 in
//the tutorial. If it is greater than zero adjust the node's weights
//accordingly
for (int n=0; n<m_SOM.size(); ++n)
{
//calculate the Euclidean distance (squared) to this node from the
//BMU
double DistToNodeSq = (m_pWinningNode->X()-m_SOM[n].X()) * (m_pWinningNode->X()-m_SOM[n].X()) +
(m_pWinningNode->Y()-m_SOM[n].Y()) * (m_pWinningNode->Y()-m_SOM[n].Y()) ;

double WidthSq = m_dNeighbourhoodRadius * m_dNeighbourhoodRadius;

//if within the neighbourhood adjust its weights
if (DistToNodeSq < (m_dNeighbourhoodRadius * m_dNeighbourhoodRadius))
{

//calculate by how much its weights are adjusted
m_dInfluence = exp(-(DistToNodeSq) / (2*WidthSq));

m_SOM[n].AdjustWeights(data[ThisVector],
m_dLearningRate,
m_dInfluence);
}

}//next node


//reduce the learning rate
m_dLearningRate = constStartLearningRate * exp(-(double)m_iIterationCount/m_iNumIterations);

++m_iIterationCount;


for (int w=0; w<target.size(); ++w)
{
m_dWeights[w] += LearningRate * Influence * (target[w] - m_dWeights[w]);
}


 */ 